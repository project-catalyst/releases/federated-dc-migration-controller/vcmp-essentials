#!/usr/bin/env bash

# Working directory
cd /home/ubuntu

# Create env file for DCMC Lite Client
echo "DCMCL_DC_HOST_TYPE=${1}" > /home/ubuntu/.env
echo "DCMCL_PHYSICAL_NI=${2}" >> /home/ubuntu/.env
echo "DCMCL_KEYCLOAK_USERNAME=${3}" >> /home/ubuntu/.env
echo "DCMCL_KEYCLOAK_PASSWORD=${4}" >> /home/ubuntu/.env
echo "DCMCL_COVPN_HOST_URL=${5}" >> /home/ubuntu/.env
echo "DCMCL_COVPN_OWNER=${6}" >> /home/ubuntu/.env
echo "DCMCL_COVPN_TENANT=${7}" >> /home/ubuntu/.env
echo "DCMCL_DCMCS_HOST_URL=${8}" >> /home/ubuntu/.env
echo "DCMCL_TRANSACTION_ID=${9}" >> /home/ubuntu/.env
echo "DCMCL_TUN_DEVICE=tun0" >> /home/ubuntu/.env

# Generate SSH Keys if they do not exist
# Touch .ssh/authorized_keys file in order to later other computes
if [ "${1}" == "all_in_one:virtual" ]; then
    # DevStack / All-in-One Installation
    # Create stack user and add to sudoers if all_in_one installation
    sudo useradd -s /bin/bash -d /opt/stack -u 1500 -m stack
    echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
    # Generate SSH Keys, touch .ssh/authorized_keys, set SSH dir
    sudo -u stack -H sh -c "cat /dev/zero | ssh-keygen -q -N ''"
    sudo -u stack -H sh -c "touch /opt/stack/.ssh/authorized_keys"
    echo "DCMCL_SSH_KEY_DIR=/opt/stack/.ssh" >> /home/ubuntu/.env
else
    # OpenStack / Distributed Installation
    # Generate SSH Keys, touch .ssh/authorized_keys, set SSH dir
    cat /dev/zero | ssh-keygen -q -N ""
    sudo touch /home/ubuntu/.ssh/authorized_keys
    echo "DCMCL_SSH_KEY_DIR=/home/ubuntu/.ssh" >> /home/ubuntu/.env
fi

# Generate SSH Keys, touch .ssh/known_hosts
sudo -u root -H sh -c "cat /dev/zero | ssh-keygen -q -N ''"
sudo -u root -H sh -c "touch /root/.ssh/known_hosts"

# Add `nova` and `neutron` users to sudoers
echo "nova ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/nova
echo "neutron ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/neutron

# Wait until docker daemon is up
while ! docker ps | grep "CONTAINER" ; do
  sleep 2
done

# Run DCMC Lite Client
docker-compose -p 'catalyst' up -d

# Wait until controller IP is written
( tail -f -n0 /etc/hosts & ) | grep -q "controller"

# In case of DevStack, create link between /var/lib/nova to /opt/stack/data/nova
if [ "${1}" == "all_in_one:virtual" ]; then
    sudo mkdir /opt/stack/data
    sudo mkdir /opt/stack/data/nova
    sudo ln -s /var/lib/nova/instances /opt/stack/data/nova
    sudo ln -s /var/lib/neutron /opt/stack/data
fi

# Stop services
sudo service nova-compute stop
sudo service libvirtd stop
sudo service libvirt-guests stop
sudo service neutron-openvswitch-agent stop

# Change essential UIDs and GIDs
sudo -u root -H sh -c "id -u nova | xargs -I % sh -c 'usermod -u $(cat os_config/uid/nova.txt) nova; find / -uid % -exec chown nova {} \;'"
sudo -u root -H sh -c "id -g nova | xargs -I % sh -c 'groupmod -g $(cat os_config/gid/nova.txt) nova; find / -gid % -exec chgrp nova {} \;'"
sudo -u root -H sh -c "id -u libvirt-qemu | xargs -I % sh -c 'usermod -u $(cat os_config/uid/libvirt-qemu.txt) libvirt-qemu; find / -uid % -exec chown libvirt-qemu {} \;'"
sudo -u root -H sh -c "id -g libvirt-qemu | xargs -I % sh -c 'groupmod -g $(cat os_config/gid/kvm.txt) kvm; find / -gid % -exec chgrp kvm {} \;'"
sudo -u root -H sh -c "groupmod -g $(cat os_config/gid/libvirt.txt) libvirt; find / -gid 1501 -exec chgrp libvirt {} \;"

# Copy configs
sudo cp os_config/nova/temp_nova.conf /etc/nova/nova.conf
sudo cp os_config/neutron/temp_neutron.conf /etc/neutron/neutron.conf
sudo cp os_config/neutron/plugins/ml2/temp_openvswitch_agent.ini /etc/neutron/plugins/ml2/openvswitch_agent.ini
sudo cp os_config/sysctl/sysctl.conf /etc/sysctl.conf

# Change ownership of some directories
sudo chown -R root:nova /etc/nova
sudo chown -R root:root /etc/nova/root*
sudo chown -R neutron:neutron /etc/neutron
sudo chown -R root:root /etc/neutron/root*

# Mount shared storage
sudo chmod o+x /var/lib/nova/instances
sudo chown -R nova:root /var/lib/nova/instances
sudo chmod o+x /opt/stack/data/nova/instances
sudo chown -R nova:root /opt/stack/data/nova/instances
sudo mount -a -v

# Restart services
sudo sysctl -p
sudo service nova-compute restart
sudo service libvirtd restart
sudo service libvirt-guests restart
sudo service neutron-openvswitch-agent restart

# Wait for creation of VXLAN interface to set remote_ip
while ! sudo ovs-vsctl list-ports br-tun | grep "vxlan" ; do
    sleep 1
done
sudo ovs-vsctl -- set interface $(sudo ovs-vsctl list-ports br-tun | grep "vxlan") type=vxlan options:remote_ip=$(sudo cat /etc/hosts | grep "controller" | head -1 | awk '{print $1}')
