#!/bin/bash

# Stop services
sudo service nova-compute stop
sudo service neutron-openvswitch-agent stop
sudo service libvirtd stop
sudo service libvirt-guests stop

# Clean lists and cache
sudo rm -rf /var/lib/apt/lists/*
sudo rm -rf /home/ubuntu/.cache

# Truncate log files
sudo truncate -s 0 /var/log/*log
sudo truncate -s 0 /var/log/*/*log
sudo truncate -s 0 /var/log/*/*/*log
sudo truncate -s 0 /var/log/*/*/*/*log

# Clean history
history -c

# Shutdown
sudo shutdown now
