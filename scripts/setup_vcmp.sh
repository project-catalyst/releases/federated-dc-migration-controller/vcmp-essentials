#!/usr/bin/env bash

# Install docker
# Perform an update and install essentials
sudo apt-get update
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common

# Add docker key and repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"

# Perform another update and install docker
sudo apt-get update
sudo apt-get install -y docker-ce

# Add user to docker group
sudo groupadd docker
sudo usermod -aG docker $USER

# Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

# Download DCMCL Image
sudo docker pull powerops/catalyst-dcmc-lite

# Install Nova-compute
sudo apt dist-upgrade -y
sudo apt install -y ntp nfs-common software-properties-common
sudo add-apt-repository -y cloud-archive:train
sudo apt update && sudo apt dist-upgrade -y
sudo apt install -y python3-openstackclient nova-compute neutron-openvswitch-agent
