# Essential vCMP files

This repository includes the necessary files for the the creation and deployment
of a virtual  Compute (vCMP) node. Installation scripts, cloud-init script and 
essential configuration files are included in this repository. 

The snapshot for the latest version of vCMP (vCompute-Train-v1.0.0-SNAPSHOT) is 
built on top of Ubuntu Server 18.04 (Bionic Beaver). Different versions of the
vCMP image will be built and uploaded shortly. The images will include versions
with both Openvswitch and Linuxbridge as Neutron agents, as well as, ideally, 
different versions of OpenStack (e.g. Queens, Rocky, Stein, Train).

## Scripts

Two scripts are included under the **scripts** directory (no surprise here ...),
**setup_vcmp.sh** and **clean_for_snapshot.sh**.

The **setup_vcmp.sh** installs the required packages for the deployment, and
includes the following steps:

  * Installation of [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) engine and [docker-compose](https://docs.docker.com/compose/);
  * Pull of the [powerops/catalyst-dcmc-lite](https://hub.docker.com/r/powerops/catalyst-dcmc-lite), i.e. the docker image for the DCMC Lite CLient;
  * Installation of [ntp](https://packages.ubuntu.com/bionic/ntp) and [nfs-common](https://packages.ubuntu.com/bionic/nfs-common) for shared storage;
  * Addition of the [train](https://wiki.ubuntu.com/OpenStack/CloudArchive#Train) repository, update and dist-upgrade;
  * Installation of [python3-openstackclient](https://packages.ubuntu.com/bionic/python3-openstackclient), [nova-compute](https://packages.ubuntu.com/bionic/nova-compute) and [neutron-openvswitch-agent](https://packages.ubuntu.com/bionic/neutron-openvswitch-agent);

When this script is executed, the requirements for the deployment of compute node
have been installed. Of course, this script can be altered and used in case anyone
wishes to build his / her own vCMP image, or generally for installation of compute
pre-requisites. The current version of the script will install the latest Train
packages.

The purpose of the **clean_for_snapshot.sh** script, with a descriptive title, is
to clean the (virtual) machine before creating a snapshot. It includes the following
steps:

  * Stops services (nova, neutron, libvirt);
  * Cleans the `/var/lib/apt/lists/*` and `/home/ubuntu/.cache` directories;
  * Truncates log files lying in the `/var/log/*` directory;
  * Cleans command history and shuts down the VM.

After this script is executed, the machine is shut down and a snapshot can be created
without the existence of unecessary logs and other elements.


## Cloud-init Script

The cloud-init script, entitled **dcmcl_setup.sh** (standing for DCMC Lite Setup) is
the script that is executed when a vCMP is brought up. It performs essential
actions that conclude to the registration of vCMP as a Compute node in OpenStack,
as well a Neutron agent (currently Openvswitch only, yet Linuxbridge is going to
be supported shortly).

The arguments that are passed to the cloud-init script are described in the following table. 
The arguments should be given to cloud-init *strictly* in the following order:

| # | Argument | Tag | Description |
| - | -------- | --- | ----------- |
| 1 | DCMCL_DC_HOST_TYPE | `DCMC Lite` | The host type in the form <deployment_type>:<host_type>. Possible values: deployment_type: {all_in_one, distributed}, host_type: {physical, virtual} | 
| 2 | DCMCL_PHYSICAL_NI | `DCMC Lite` | Physical NW iface |
| 3 | DCMCL_KEYCLOAK_USERNAME | `Keycloak` | Username |
| 4 | DCMCL_KEYCLOAK_PASSWORD | `Keycloak` | User Password |
| 5 | DCMCL_COVPN_HOST_URL | `Cloud OpenVPN` | Host URL |
| 6 | DCMCL_COVPN_OWNER | `Cloud OpenVPN` | VPN Owner  |
| 7 | DCMCL_COVPN_TENANT | `Cloud OpenVPN` | Tenant / Project |
| 8 | DCMCL_DCMCS_HOST_URL | `DCMC Server` | Host URL  |
| 9 | DCMCL_TRANSACTION_ID | `Marketplace` | Transaction ID |

The actions that are executed by the cloud-init script are as follows:

  * The aforementioned arguments are saved in an .env file for Docker DCMC Lite deployment;
  * In case of a `all_in_one:virtual` deployment, a `stack` user is created and SSH keys are generated, otherwise SSH keys are generated for the default user (e.g. `ubuntu`);
  * SSH keys are generated for the `root` user;
  * When the `Docker` daemon is up, the DCMC Lite Client is deployed;
  * DevStack & OpenStack save instances in different directories, hence in case of a DevStack source DC, symbolic links must be created for the Neutron and Nova directories;
  * When DCMC Lite Client has performed preparatory tasks, including the configuration of Nova & Neutron, the update of /etc/hosts, etc., the OpenStack related services are stopped;
  * The GIDs and UIDs of `nova` & `libvirt-qemu` users are altered to match the ones of the source DC (for NFS shared storage access);
  * The configuration files of `nova` and `neutron` are updated;
  * The NFS shared storage is configured and mounted;
  * The OpenStack-related services, i.e. `nova-compute`, `neutron-openvswitch-agent`, `libvirtd`, `libvirt-guests` are restarted;
  * The script finally waits until the live-migration is performed to configure the remote IPs of the VXLAN interface.

These are all the steps that are executed by the cloud-init script, from the initiation of the vCMP to the actual live-migration. 
Part of this script could actually be used in order to deploy an OpenStack CMP node.


## OpenStack Config Files

In this directory, `os_config` lie the essential configuration files for the 
`nova-compute` and `neutron-openvswitch-agent` services (`libvirt` is 
already pre-configured) in the snapshot, as of version v1.0.0. The structure of
this directory is the following:

```
os_config
├── gid
│   ├── kvm.txt
│   ├── libvirt.txt
│   └── nova.txt
├── neutron
│   ├── neutron.conf
│   └── plugins
│       └── ml2
│           └── openvswitch_agent.ini
├── nova
│   └── nova.conf
├── sysctl
│   └── sysctl.conf
└── uid
    ├── libvirt-qemu.txt
    └── nova.txt

```

As obvious as it might be, this directory includes:

  * a `Nova` confiration template;
  * `Neutron` configuration templates;
  * `sysctl` configuration file;
  * a `uid` directory where the UIDs of `nova` and `libvirt-qemu` are written;
  * a `gid` directory where the GIDs of `nova` and `kvm` are written.

Some of these files are edited after vCMP deployment in order for the OpenStack-related services to be configured.

## Usefull Links

  * [OpenStack Live-Migration Flow](http://bodenr.blogspot.com/2014/03/openstack-nova-vm-migration-live-and.html): An extremely useful blogpost, depicting the flow of function calls for a live-migration to take place. Though originally written in 2014, it seemsthat the whole live-migration flow remains the same, at least for the most critical parts, hence it really helped the debugging procedures.
  * [Configure live migrations](https://docs.openstack.org/nova/pike/admin/configuring-migrations.html): Instructions from the OpenStack docs on essential configurations for a live-migration to take place.
  * [Install and configure a compute node](https://docs.openstack.org/nova/train/install/compute-install-ubuntu.html): Instructions from the OpenStack docs on how to install and configure a compute node. It proved non-trivial as there were issues with the Cells. Please refer for an overview of cell configuration in a new OpenStack/DevStack installation. Check compatibility of service versions.
  * [Network Troubleshooting](https://wiki.openstack.org/wiki/OpsGuide/Network_Troubleshooting): OpenStack Wiki page for Network Troubleshooting, which proved really useful.
